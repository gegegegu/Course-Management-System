-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: class
-- ------------------------------------------------------
-- Server version	5.7.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adname` varchar(45) DEFAULT NULL,
  `password` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'ge','123456'),(2,'bei','123456');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `course` (
  `course_id` int(4) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(100) DEFAULT NULL,
  `type` varchar(8) DEFAULT NULL,
  `studytime` int(3) DEFAULT NULL,
  `check` char(4) DEFAULT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'JAVA程序设计','操作性',32,'必修'),(2,'HTML','操作性',16,'必修'),(3,'Pr','操作性',20,'必修'),(4,'操作系统','理论性',22,'必修'),(5,'数据结构','综合性',34,'必修'),(6,'3ds max','操作性',12,'必修'),(7,'C语言','操作性',20,'必修'),(8,'线性代数','理论性',16,'必修'),(9,'大学英语','理论性',16,'必修'),(10,'大学生创新创业','线上',8,'选修'),(11,'书法鉴赏','线上',8,'选修'),(12,'古诗词里爱情观与婚姻观','理论性',8,'选修'),(13,'幸福心理学','理论性',8,'选修'),(14,'健美操','实践性',8,'选修'),(15,'高尔夫','实践性',8,'选修'),(16,'FLASH动画','操作性',20,'必修'),(17,'图像处理','操作性',20,'必修');
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_course`
--

DROP TABLE IF EXISTS `p_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `p_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(4) DEFAULT NULL,
  `course_id` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_course`
--

LOCK TABLES `p_course` WRITE;
/*!40000 ALTER TABLE `p_course` DISABLE KEYS */;
INSERT INTO `p_course` VALUES (1,1,1),(2,1,2),(3,1,5),(4,1,8),(5,1,9),(6,2,1),(7,2,2),(8,2,7),(9,2,8),(10,2,9),(11,3,2),(12,3,3),(13,3,6),(14,3,8),(15,3,9),(16,4,2),(17,4,16),(18,4,17),(19,4,8),(20,4,9);
/*!40000 ALTER TABLE `p_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `positional`
--

DROP TABLE IF EXISTS `positional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `positional` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `positional` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `positional`
--

LOCK TABLES `positional` WRITE;
/*!40000 ALTER TABLE `positional` DISABLE KEYS */;
INSERT INTO `positional` VALUES (1,'计算机科学与技术'),(2,'软件工程'),(3,'数字媒体技术'),(4,'动画制作');
/*!40000 ALTER TABLE `positional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student` (
  `stu_id` int(12) NOT NULL AUTO_INCREMENT,
  `stu_name` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `positional` varchar(30) DEFAULT NULL,
  `grade` char(2) DEFAULT NULL,
  `sex` char(2) DEFAULT NULL,
  `password` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`stu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2019059310 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (202005920,'玉祁','2000-04-02','1','20','男','987456'),(2018059204,'谷会格','1999-03-05','1','18','女','123456'),(2018059205,'谭亚茹','1999-12-03','1','18','女','321654'),(2018059207,'王尔','1999-05-18','2','18','男','321654'),(2018059208,'路十一','1998-08-23','2','18','男','321654'),(2019059203,'王艳楠','2000-11-02','3','18','女','123456'),(2019059301,'王慧慧','2001-10-16','4','19','女','123456'),(2019059302,'程贝贝','2000-05-16','3','19','女','123456'),(2019059306,'王瑾','2000-01-01','2','19','女','321654'),(2019059309,'欧柳','1999-09-03','1','19','男','987456');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teacher` (
  `tea_id` int(8) NOT NULL AUTO_INCREMENT,
  `tea_name` varchar(50) DEFAULT NULL,
  `sex` char(2) DEFAULT NULL,
  `bir_date` date DEFAULT NULL,
  `course_id` int(4) DEFAULT NULL,
  `password` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`tea_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher`
--

LOCK TABLES `teacher` WRITE;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
INSERT INTO `teacher` VALUES (1,'清华','男','1989-03-06',1,'123456'),(2,'北大','女','1990-06-07',2,'123456'),(3,'复旦','男','1995-09-07',3,'123456'),(4,'浙大','男','1993-10-21',4,'123456'),(5,'南开','女','1992-05-24',5,'123456'),(6,'路十一','男','1988-10-23',6,'123456'),(7,'李世柳','女','1993-07-12',7,'123456'),(8,'王尔','女','1990-02-19',8,'123456'),(9,'高崎','男','1979-04-09',9,'123456'),(10,'吴宇','男','1994-08-06',10,'123456');
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-20 19:42:39
