package cla.dao;

import java.util.List;

import cla.entity.Admin;
import cla.entity.Course;
import cla.entity.Student;
import cla.entity.Teacher;

public interface AdminDao {
	Admin getByName(String adname);
	List<Student> getAllStu();
	List<Course> getAllCou();
	List<Teacher> getAllTea();
	int delByStuID(int id);
	int delByTeaID(int id);
	int delByCourID(int id);

	int upByStuID(int id,String name,String sex,String birthday,String password);
	int upByTeaID(int id,String name,String sex,String birthday,String password);
	int upByCourID(int id,String name,String type,String check,int studytime);
}
