package cla.dao;

import java.util.List;


import cla.entity.Course;
public interface ClourseDao {
	List<Course> getStuAllCourses(int pid);
	Course getByCourse(String clourseName,String type);
	Course getById(int id);
}
