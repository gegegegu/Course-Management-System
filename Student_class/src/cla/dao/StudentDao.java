package cla.dao;

import cla.entity.Student;

public interface StudentDao {
	Student getByID(int id);
	Student getByStudent(String name,String course);
}
