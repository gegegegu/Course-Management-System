package cla.dao;

import java.util.List;

import cla.entity.Student;
import cla.entity.Teacher;

public interface TeacherDao {
	Teacher getByID(int tea_id);
	List<Student> getAllCouStu(int tea_id);
}
