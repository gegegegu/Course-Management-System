package cla.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import cla.dao.AdminDao;
import cla.entity.Admin;
import cla.entity.Course;
import cla.entity.Student;
import cla.entity.Teacher;
import cla.util.DBUtil;

public class AdminDaoImpl implements AdminDao{

	@Override
	public Admin getByName(String adname) {
		// TODO 自动生成的方法存根
		String sql="select * from admin where adname='"+adname+"'";
        ResultSet rs=DBUtil.query(sql);
        Admin a=new Admin();
        try {
            if (rs.next()) {
                a.setAdname(adname);;
                a.setPassword(rs.getString("password"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AdminDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
		return a;
	}

	@Override
	public List<Student> getAllStu() {
		// TODO 自动生成的方法存根
		String sql="select s.stu_id,s.stu_name,s.sex,s.birthday,s.grade,s.password,p.positional from student as s,positional as p where p.id=s.positional";
        ResultSet rs=DBUtil.query(sql);  
        List<Student>list=new ArrayList<Student>();
        try {
            while (rs.next()) {
                Student s=new Student();
                s.setPassword(rs.getString("password"));
                s.setStuName(rs.getString("stu_name"));
                s.setSex(rs.getString("sex"));
                s.setStuId(rs.getInt("stu_id"));
                s.setGrade(rs.getInt("grade"));
                s.setPositional(rs.getString("positional"));
                s.setBirthday(rs.getString("birthday"));
                list.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AdminDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
	}

	@Override
	public List<Course> getAllCou() {
		// TODO 自动生成的方法存根
		String sql="select c.course_id,c.course_name,c.type,c.studytime,c.check,t.tea_name from course as c,teacher as t where t.course_id=c.course_id";
        ResultSet rs=DBUtil.query(sql);  
        List<Course>list=new ArrayList<Course>();
        try {
            while (rs.next()) {
            	Course c=new Course();
                c.setType(rs.getString("type"));
                c.setCourse_name(rs.getString("course_name"));
                c.setCourse_id(rs.getInt("course_id"));
                c.setStudytime(rs.getInt("studytime"));
                c.setCheck(rs.getString("check"));
                c.setTea_name(rs.getString("tea_name"));
                list.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AdminDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
	}

	@Override
	public List<Teacher> getAllTea() {
		// TODO 自动生成的方法存根
		String sql="select t.tea_id,t.tea_name,t.sex,t.bir_date,t.password,c.course_name from course as c,teacher as t where t.course_id=c.course_id";
        ResultSet rs=DBUtil.query(sql);  
        List<Teacher>list=new ArrayList<Teacher>();
        try {
            while (rs.next()) {
            	Teacher t=new Teacher();
                t.setSex(rs.getString("sex"));
                t.setTea_name(rs.getString("tea_name"));
                t.setTea_id(rs.getInt("tea_id"));
                t.setBirthday(rs.getString("bir_date"));
                t.setCourse_name(rs.getString("course_name"));
                t.setPassword(rs.getString("password"));
                list.add(t);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AdminDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
	}

	@Override
	public int delByStuID(int id) {
		// TODO 自动生成的方法存根
		String sql="delete from student where stu_id="+id;
        int result=DBUtil.update(sql);
        return result;
	}

	@Override
	public int delByTeaID(int id) {
		// TODO 自动生成的方法存根
		String sql="delete from teacher where tea_id="+id;
        int result=DBUtil.update(sql);
        return result;
	}

	@Override
	public int delByCourID(int id) {
		// TODO 自动生成的方法存根
		String sql="delete from course where course_id="+id;
        int result=DBUtil.update(sql);
        return result;
	}

	@Override
	public int upByStuID(int id,String name, String sex, String birthday, String password) {
		// TODO 自动生成的方法存根
		String sql="update student set stu_name='"+name+"',sex='"+sex+"',birthday='"+birthday+"',password='"+password+"' where stu_id="+id;
        int result=DBUtil.update(sql);
        return result;
	}

	@Override
	public int upByTeaID(int id,String name, String sex, String birthday, String password) {
		// TODO 自动生成的方法存根
		String sql="update teacher set tea_name='"+name+"',sex='"+sex+"',bir_date='"+birthday+"',password='"+password+"' where tea_id="+id;
        System.out.print("11111");
		int result=DBUtil.update(sql);
        return result;
	}

	@Override
	public int upByCourID(int id,String name, String type, String check, int studytime) {
		// TODO 自动生成的方法存根
		String sql="update course set course_name='"+name+"',type='"+type+"',check='"+check+"',student='"+studytime+"' where course_id="+id;
        int result=DBUtil.update(sql);
        return result;
	}


}
