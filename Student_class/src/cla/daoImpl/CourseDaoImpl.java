package cla.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



import cla.entity.Course;
import cla.util.DBUtil;
import cla.dao.ClourseDao;


public class CourseDaoImpl implements ClourseDao{

	@Override
	public List<Course> getStuAllCourses(int pid) {
		// TODO 自动生成的方法存根
		String sql="select c.course_id,c.course_name,c.type,c.studytime,c.check,teacher.tea_name from course as c,positional,p_course,teacher where positional.id=p_course.p_id and p_course.course_id=c.course_id and teacher.course_id=c.course_id and positional.id="+pid;
        ResultSet rs=DBUtil.query(sql);  
        List<Course>list=new ArrayList<Course>();
        try {
            while (rs.next()) {
                Course c=new Course();
                c.setCourse_id(rs.getInt("course_id"));
                c.setCourse_name(rs.getString("course_name"));
                c.setType(rs.getString("type"));
                c.setStudytime(rs.getInt("studytime"));
                c.setCheck(rs.getString("check"));
                c.setTea_name(rs.getString("tea_name"));
                list.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CourseDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
	}

	@Override
	public Course getByCourse(String courseName, String type) {
		// TODO 自动生成的方法存根
		String sql="select c.course_name,c.type,c.studytime,c.check,t.tea_name from course as c,teacher as t where c.course_id=t.course_id and c.course_name='"+courseName+"' and c.check='"+type+"'";
        ResultSet rs=cla.util.DBUtil.query(sql);
        Course c=new Course();
        try {
            if (rs.next()) {
                c.setCourse_name(courseName);
                c.setType(type);
                c.setTea_name(rs.getString("tea_name"));
                c.setCheck(rs.getString("check"));
                c.setStudytime(rs.getInt("studytime"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(CourseDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return c;
	}

	@Override
	public Course getById(int id) {
		// TODO 自动生成的方法存根
		String sql="select * from course where course_id="+id;
        ResultSet rs=DBUtil.query(sql);
        Course c=new Course();
        try {
        	if (rs.next()) {
                c.setCourse_id(id);
                c.setCourse_name(rs.getString("course_name"));
                c.setType(rs.getString("type"));
                c.setCheck(rs.getString("check"));
                c.setStudytime(rs.getInt("studytime"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return c;
	}

	
	
}
