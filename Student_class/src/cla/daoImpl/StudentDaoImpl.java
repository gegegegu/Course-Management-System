package cla.daoImpl;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import cla.dao.StudentDao;
import cla.entity.Student;
import cla.util.DBUtil;

public class StudentDaoImpl implements StudentDao{

	@Override
	public Student getByID(int id) {
		// TODO 自动生成的方法存根
		String sql="select * from student where stu_id="+id;
        ResultSet rs=DBUtil.query(sql);
        Student s=new Student();
        try {
        	if (rs==null) {
				System.out.println("用户名错误");
			}else if (rs.next()) {
                s.setStuId(id);
                s.setStuName(rs.getString("stu_name"));
                s.setBirthday(rs.getString("birthday"));
                s.setP_id(rs.getInt("positional"));
                s.setSex(rs.getString("sex"));
                s.setStuName(rs.getString("stu_name"));
                s.setGrade(rs.getInt("grade"));
                s.setPassword(rs.getString("password"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return s;
	}

	@Override
	public Student getByStudent(String name, String cla) {
		// TODO 自动生成的方法存根
		String sql="select s.stu_id,s.stu_name,s.sex,s.birthday,s.grade,c.class_name from student as s,p_class,class as c where s.positional=p_class.p_id and p_class.class_id=c.class_id and s.stu_name='"+name+"' and c.class_name='"+cla+"'";
        ResultSet rs=DBUtil.query(sql);
        Student s=new Student();
        try {
            if (rs.next()) {
                s.setStuId(rs.getInt("stu_id"));
                s.setStuName(rs.getString("stu_name"));
                s.setBirthday(rs.getString("birthday"));
                s.setCourse_name(rs.getString("class_name"));
                s.setSex(rs.getString("sex"));
                s.setStuName(rs.getString("stu_name"));
                s.setGrade(rs.getInt("grade"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return s;
	}

}
