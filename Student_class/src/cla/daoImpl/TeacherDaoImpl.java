package cla.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import cla.dao.TeacherDao;
import cla.entity.Student;
import cla.entity.Teacher;
import cla.util.DBUtil;

public class TeacherDaoImpl implements TeacherDao{

	@Override
	public Teacher getByID(int tea_id) {
		// TODO 自动生成的方法存根
		String sql="select * from teacher where tea_id='"+tea_id+"'";
        ResultSet rs=DBUtil.query(sql);
        Teacher t=new Teacher();
        try {
            if (rs.next()) {
                t.setTea_id(tea_id);
                t.setTea_name(rs.getString("tea_name"));
                t.setSex(rs.getString("sex"));
                t.setBirthday(rs.getString("bir_date"));
                t.setCourse_id(rs.getInt("course_id"));
                t.setPassword(rs.getString("password"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(CourseDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return t;
	}

	@Override
	public List<Student> getAllCouStu(int tea_id) {
		// TODO 自动生成的方法存根
		String sql="select s.stu_id,s.stu_name,s.birthday,s.grade,p.positional,c.course_name "
				+ "from teacher as t,p_course,student as s,positional as p,course as c "
				+ "where t.course_id=p_course.course_id and p_course.p_id=s.positional and p.id=p_course.p_id and c.course_id=p_course.course_id and t.tea_id="+tea_id;
        ResultSet rs=DBUtil.query(sql);  
        List<Student>list=new ArrayList<Student>();
        try {
            while (rs.next()) {
                Student s=new Student();
                s.setCourse_name(rs.getString("course_name"));
                s.setStuName(rs.getString("stu_name"));
                s.setStuId(rs.getInt("stu_id"));
                s.setGrade(rs.getInt("grade"));
                s.setPositional(rs.getString("positional"));
                s.setBirthday(rs.getString("birthday"));
                list.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CourseDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
	}

}
