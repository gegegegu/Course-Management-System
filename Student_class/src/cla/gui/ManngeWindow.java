package cla.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;


import cla.daoImpl.AdminDaoImpl;
import cla.entity.Course;
import cla.entity.Student;
import cla.entity.Teacher;

public class ManngeWindow extends JFrame implements ActionListener{
    private JLabel jLabel;
   
    private JButton jButton;
    private JRadioButton jRadioButton;
    private JRadioButton jRadioButton1;
    private JRadioButton jRadioButton2;
    private ButtonGroup buttonGroup;
    
    private JTable table;
    private DefaultTableModel model;
    private TableColumn column = null;
    private JScrollPane scroll;
   
    public ManngeWindow(){
        InitComp();
    }
    public void InitComp(){
    	setBounds(400,100,700,700);
        setTitle("课程管理");
        setLayout(null);
        jLabel=new JLabel("管理员后台页面");
        jLabel.setBounds(260, 30, 190, 50);
        jLabel.setFont( (new Font("楷体",Font.BOLD,26)));
        add(jLabel);
       
        jButton=new JButton("返回");
        jButton.setBounds(550, 80, 100, 30);
        jButton.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton.setBackground(Color.orange);
        jButton.addActionListener(this);
        add(jButton);
        
        buttonGroup =new ButtonGroup();
        jRadioButton=new JRadioButton("学生");
        jRadioButton.setBounds(170,130,150,30);
        jRadioButton.setFont( (new  Font("楷体",Font.BOLD,15)));
        jRadioButton.addActionListener(this);
        buttonGroup.add(jRadioButton);
        
        jRadioButton1=new JRadioButton("老师");
        jRadioButton1.setBounds(320,130,150,30);
        jRadioButton1.setFont( (new  Font("楷体",Font.BOLD,15)));
        jRadioButton1.addActionListener(this);
        buttonGroup.add(jRadioButton1);
        
        jRadioButton2=new JRadioButton("课程");
        jRadioButton2.setBounds(470,130,150,30);
        jRadioButton2.setFont( (new  Font("楷体",Font.BOLD,15)));
        jRadioButton2.addActionListener(this);
        buttonGroup.add(jRadioButton2);
        
        add(jRadioButton);
        add(jRadioButton1);
        add(jRadioButton2);
                
        model =new DefaultTableModel();
        table = new JTable(model);
        table.getTableHeader().setPreferredSize(new Dimension(1,40));
        table.getTableHeader().setFont(new Font("楷体",Font.CENTER_BASELINE,16));
        table.setRowHeight(40);
	    
        table.setFont( (new  Font("楷体",Font.CENTER_BASELINE,16)));
	    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); 
		 
	          
	    scroll = new JScrollPane(table);  
	    scroll.setBounds(20,180,650,450);
	    add(scroll);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new ManngeWindow();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	
        if(e.getSource()==jRadioButton){
            model.setRowCount(0);
            model.setColumnCount(7);
    		String[] columnNames= {"学号","姓名","性别","出生日期","年级","专业","账号密码"};
            model.setColumnIdentifiers(columnNames);
        	List<Student> list =new ArrayList<Student>();
            list=new AdminDaoImpl().getAllStu();
            for (int i = list.size()-1; i >= 0; i--) {
    			Student s=(Student)list.get(i);
    			Object[] obj={s.getStuId(),s.getStuName(),s.getSex(),s.getBirthday(),s.getGrade(),s.getPositional(),s.getPassword()};
    			model.insertRow(table.getSelectedRowCount(), obj);
    		}
            int colunms = table.getColumnCount();  
    	    for(int j = 0; j < colunms; j++)  
    	    {  
    	        column = table.getColumnModel().getColumn(j);  
    	        column.setPreferredWidth(120);  
    	     }  
            
        }else if(e.getSource()==jRadioButton1){
            model.setRowCount(0);
            model.setColumnCount(6);
    		String[] columnNames= {"教师工号","姓名","性别","出生日期","教授课程","账号密码"};
            model.setColumnIdentifiers(columnNames);
        	List<Teacher> list =new ArrayList<Teacher>();
            list=new AdminDaoImpl().getAllTea();
            for (int i = list.size()-1; i >= 0; i--) {
    			Teacher t=(Teacher)list.get(i);
    			Object[] obj={t.getTea_id(),t.getTea_name(),t.getSex(),t.getBirthday(),t.getCourse_name(),t.getPassword()};
    			model.insertRow(table.getSelectedRowCount(), obj);
    		}
            int colunms = table.getColumnCount();  
    	    for(int j = 0; j < colunms; j++)  
    	    {  
    	        column = table.getColumnModel().getColumn(j);  
    	        column.setPreferredWidth(120);  
    	     }  
            
        }else if(e.getSource()==jRadioButton2){
            model.setRowCount(0);
            model.setColumnCount(6);
    		String[] columnNames= {"课程号","课程名称","课程类型","授课教师","学时","选修、必修"};
            model.setColumnIdentifiers(columnNames);
        	List<Course> list =new ArrayList<Course>();
            list=new AdminDaoImpl().getAllCou();
            for (int i = list.size()-1; i >= 0; i--) {
    			Course c1=(Course)list.get(i);
    			Object[] obj={c1.getCourse_id(),c1.getCourse_name(),c1.getType(),c1.getTea_name(),c1.getStudytime(),c1.getCheck()};
    			model.insertRow(table.getSelectedRowCount(), obj);
    		}
            int colunms = table.getColumnCount();  
    	    for(int j = 0; j < colunms; j++)  
    	    {  
    	        column = table.getColumnModel().getColumn(j);  
    	        column.setPreferredWidth(120);  
    	     }  
            
        }
        
        if (e.getSource()==jButton) {
			new Admin_Login();
			dispose();
		}
        
    }
}


