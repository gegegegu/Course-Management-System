package cla.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import cla.daoImpl.TeacherDaoImpl;
import cla.entity.Student;

public class StuOfCourse extends JFrame implements ActionListener{
	private JLabel jLabel;
    private JButton jButton2;
	public StuOfCourse(){
        InitComp();
    }
    public void InitComp(){
    	setBounds(400,100,700,700);
        setTitle("课程管理");
        setLayout(null);
        
        jLabel=new JLabel("学习本门课程的学生信息");
        jLabel.setFont( (new  Font("楷体",Font.BOLD,26)));
        jLabel.setBounds(250, 50, 190, 50);
        add(jLabel);
        
        DefaultTableModel model =new DefaultTableModel(0,6);
        JTable table = new JTable(model);
        table.getTableHeader().setPreferredSize(new Dimension(1,40));
        table.getTableHeader().setFont(new Font("楷体",Font.CENTER_BASELINE,16));
        table.setRowHeight(40);
        
        model.setRowCount(0);
		String[] columnNames= {"学号","姓名","学习课程","出生日期","年级","专业"};
        model.setColumnIdentifiers(columnNames);
        
        
		List<Student>list=new ArrayList<Student>();
		list=new TeacherDaoImpl().getAllCouStu(Integer.parseInt(Login.uid));
		
		for (int i = list.size()-1; i >= 0; i--) {
			Student s=(Student)list.get(i);
			Object[] obj={s.getStuId(),s.getStuName(),s.getPositional(),s.getCourse_name(),s.getGrade(),s.getPositional()};
			model.insertRow(table.getSelectedRowCount(), obj);
		}

		jButton2=new JButton("返回");
        jButton2.setBounds(500,600,100,40);
        jButton2.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton2.setBackground(Color.orange);
        add(jButton2);
        jButton2.addActionListener(this);
        
	    TableColumn column = null; 
	    int colunms = table.getColumnCount(); 
	    for(int i1 = 0; i1 < colunms; i1++)  
	    {  
	        column = table.getColumnModel().getColumn(i1);  
	        column.setPreferredWidth(120);  
	     }  
         table.setFont( (new  Font("楷体",Font.CENTER_BASELINE,16)));
	     table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);  
	          
	    JScrollPane scroll = new JScrollPane(table);  
	    scroll.setBounds(20,100,650,450);
	    add(scroll);
    	setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    }
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO 自动生成的方法存根
		new Teacher_Login();
        dispose();
	}
}
