package cla.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import cla.daoImpl.AdminDaoImpl;
import cla.daoImpl.CourseDaoImpl;
import cla.daoImpl.StudentDaoImpl;
import cla.daoImpl.TeacherDaoImpl;
import cla.entity.Course;
import cla.entity.Student;
import cla.entity.Teacher;

public class Admin_Login extends JFrame implements ActionListener{
    private JLabel jLabel;
   
    private JButton jButton;
    private JButton jButton1;
    private JButton jButton2;
    private JButton jButton3;
    private JButton jButton4;
    

    public static JRadioButton jRadioButton;
    public static JRadioButton jRadioButton2;
    public static JRadioButton jRadioButton1;
    private ButtonGroup buttonGroup;
    
    private JTextField jTextField;
    private JTextArea jTextArea;
   
    public static String cid;
    public Admin_Login(){
        InitComp();
    }
    public void InitComp(){
    	setBounds(400,100,700,700);
        setTitle("课程管理");
        setLayout(null);
        jLabel=new JLabel("管理员后台页面");
        jLabel.setBounds(260, 30, 190, 50);
        jLabel.setFont( (new  Font("楷体",Font.BOLD,26)));
        add(jLabel);
       
        jButton4=new JButton("查看全部信息");
        jButton4.setBounds(100, 170, 150, 30);
        jButton4.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton4.setBackground(Color.orange);
        jButton4.addActionListener(this);
        add(jButton4);
       
        jButton3=new JButton("返回");
        jButton3.setBounds(450, 440, 100, 30);
        jButton3.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton3.setBackground(Color.orange);
        add(jButton3);
        jButton3.addActionListener(this);
       
        jTextField=new JTextField();
        jTextField.setBounds(100, 260, 330, 30);
        jTextField.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jTextField);
       
        jButton=new JButton("查询");
        jButton.setBounds(450, 260, 100, 30);
        jButton.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton.setBackground(Color.orange);
        jButton.addActionListener(this);
        add(jButton);
       
        buttonGroup=new ButtonGroup();
        jRadioButton=new JRadioButton("学生");
        jRadioButton.setBounds(110, 300, 70, 30);
        jRadioButton.setFont( (new  Font("楷体",Font.BOLD,14)));
        buttonGroup.add(jRadioButton);
        add(jRadioButton);
       
        jRadioButton1=new JRadioButton("教师");
        jRadioButton1.setBounds(250, 300, 70, 30);
        jRadioButton1.setFont( (new  Font("楷体",Font.BOLD,14)));
        buttonGroup.add(jRadioButton1);
        add(jRadioButton1);
       
        jRadioButton2=new JRadioButton("课程");
        jRadioButton2.setBounds(390, 300, 70, 30);
        jRadioButton2.setFont( (new  Font("楷体",Font.BOLD,14)));
        buttonGroup.add(jRadioButton2);
        add(jRadioButton2);
       
        jTextArea=new JTextArea();
        jTextArea.setBounds(100, 340, 450, 80);
        jTextArea.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jTextArea);
       
        jButton1=new JButton("删除信息");
        jButton1.setBounds(450, 170, 100, 30);
        jButton1.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton1.setBackground(Color.orange);
        jButton1.addActionListener(this);
        add(jButton1);
       
        jButton2=new JButton("修改");
        jButton2.setBounds(300, 170, 100, 30);
        jButton2.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton2.setBackground(Color.orange);
        jButton2.addActionListener(this);
        add(jButton2);
       
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new Admin_Login();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	
        if(e.getSource()==jButton){
        	if(jRadioButton2.isSelected()) {
        		Course c=new CourseDaoImpl().getById(Integer.parseInt(jTextField.getText()));
        		jTextArea.setText("");
        		jTextArea.append("课程名称："+c.getCourse_name()+"\n\r 课程性质："+c.getType()+"\n\r 学时："+c.getStudytime()+"\n\r 选修/必修："+c.getCheck());
        	}else if (jRadioButton.isSelected()) {
        		Student s=new StudentDaoImpl().getByID(Integer.parseInt(jTextField.getText()));
        		jTextArea.setText("");
        		jTextArea.append("学号："+s.getStuId()+"\n\r 姓名："+s.getStuName()+"\n\r 性别："+s.getSex()+"\n\r 出生日期："+s.getBirthday()+"\n\r 年级："+s.getGrade()+"\n\r 专业代码："+s.getPositional());
			}else if (jRadioButton1.isSelected()) {
        		Teacher t=new TeacherDaoImpl().getByID(Integer.parseInt(jTextField.getText()));
        		jTextArea.setText("");
        		jTextArea.append("教师工号："+t.getTea_id()+"\n\r 姓名："+t.getTea_name()+"\n\r 性别："+t.getSex()+"\n\r 出生日期："+t.getBirthday()+"\n\r 课程代码："+t.getCourse_id());
			}
        }
        
        if(e.getSource()==jButton1){
        	if(jRadioButton2.isSelected()) {
        		int result=new AdminDaoImpl().delByCourID(Integer.parseInt(jTextField.getText()));
        		if (result==1) {
        			JOptionPane.showConfirmDialog(null, "课程信息删除成功");
				}else {
					JOptionPane.showConfirmDialog(null, "课程信息删除失败");
				}
        	}else if (jRadioButton.isSelected()) {
        		int result=new AdminDaoImpl().delByStuID(Integer.parseInt(jTextField.getText()));
        		if (result==1) {
        			JOptionPane.showConfirmDialog(null, "学生信息删除成功");
				}else {
					JOptionPane.showConfirmDialog(null, "学生信息删除失败");
				}
			}else if (jRadioButton1.isSelected()) {
				int result=new AdminDaoImpl().delByTeaID(Integer.parseInt(jTextField.getText()));
        		if (result==1) {
        			JOptionPane.showConfirmDialog(null, "教师信息删除成功");
				}else {
					JOptionPane.showConfirmDialog(null, "教师信息删除失败");
				}
			}
        }
        
        if(e.getSource()==jButton3){
        	new Login();
            dispose();
        }
        
        if(e.getSource()==jButton2){

        	cid=jTextField.getText();
        	new UpWindow();
            dispose();
        }
        
        if(e.getSource()==jButton4){
        	new ManngeWindow();
            dispose();
        }
    }
}


