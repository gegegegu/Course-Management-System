package cla.gui;

import java.awt.Color;
import java.awt.Font;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


import cla.daoImpl.StudentDaoImpl;
import cla.entity.Student;

public class Teacher_Login extends JFrame implements ActionListener{
    private JLabel jLabel;
    private JLabel jLabel1;
   
    private JTextField jTextField;
    private JTextArea jTextArea;
   
    private JButton jButton;
    private JButton jButton1;
    private JButton jButton2;
   
   
    public Teacher_Login(){
        InitComp();
    }
    public void InitComp(){
        setBounds(400,100,700,700);
        setTitle("课程管理");
        setLayout(null);
        jLabel=new JLabel("教师功能页面");
        jLabel.setBounds(260, 30, 190, 50);
        jLabel.setFont( (new  Font("楷体",Font.BOLD,26)));
        add(jLabel);
       
        jLabel1=new JLabel("学    号：");
        jLabel1.setBounds(180, 130, 140, 30);
        jLabel1.setFont( (new  Font("楷体",Font.BOLD,18)));
        add(jLabel1);
       
        jTextField=new JTextField("");
        jTextField.setBounds(280,130,200,30);
        jTextField.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jTextField);
       
        jButton=new JButton("查询");
        jButton.setBounds(500,130,100,30);
        jButton.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton.setBackground(Color.orange);
        jButton.addActionListener(this);
        add(jButton);
        
        jTextArea=new JTextArea("");
        jTextArea.setFont( (new  Font("楷体",Font.BOLD,16)));
        jTextArea.setBounds(50,300,600,100);
        add(jTextArea);
        
        jButton1=new JButton("查看教授的所有学生");
        jButton1.setBounds(30,550,200,30);
        jButton1.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton1.setBackground(Color.orange);
        jButton1.addActionListener(this);
        add(jButton1);
       
        jButton2=new JButton("返回");
        jButton2.setBounds(500,550,100,30);
        jButton2.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton2.setBackground(Color.orange);
        add(jButton2);
        jButton2.addActionListener(this);
       
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new Teacher_Login();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	if(e.getSource()==jButton){
        	Student student=new StudentDaoImpl().getByStudent(jTextField.getText(), "HTML");
        	if(student.getStuName()==null) {
        		JOptionPane.showConfirmDialog(null, "该学生不存在");
        	}else {
        		jTextArea.append("学号："+student.getStuId()+"， 姓名："+student.getStuName()+"， 性别："+student.getSex()+"\n\r          出生日期："+student.getBirthday()+"， 年级："+student.getGrade()+"级， 课程名："+student.getCourse_name()+"\n\r");
        	}
        	
        }
    	
    	if(e.getSource()==jButton1){
        	new StuOfCourse();
            dispose();
        }
    	
        if(e.getSource()==jButton2){
        	new MainWindow();
            dispose();
        }
    }
}


