package cla.gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import javax.swing.table.DefaultTableModel;

import javax.swing.table.TableColumn;

import cla.daoImpl.CourseDaoImpl;
import cla.daoImpl.StudentDaoImpl;
import cla.entity.Course;
import cla.entity.Student;

public class StuAllCourse extends JFrame implements ActionListener{
    private JLabel jLabel;
    private JButton jButton2;
	public StuAllCourse(){
        InitComp();
    }
    public void InitComp(){
    	setBounds(400,100,700,700);
        setTitle("课程管理");
        setLayout(null);
        
        jLabel=new JLabel("学生课程信息");
        jLabel.setFont( (new  Font("楷体",Font.BOLD,26)));
        jLabel.setBounds(250, 50, 190, 50);
        add(jLabel);
        
        
        DefaultTableModel model =new DefaultTableModel(0,6);
        JTable table = new JTable(model);
        table.getTableHeader().setPreferredSize(new Dimension(1,40));
        table.getTableHeader().setFont(new Font("楷体",Font.CENTER_BASELINE,16));
        table.setRowHeight(40);
        
        model.setRowCount(0);
		String[] columnNames= {"课程号","课程名称","课程类型","授课教师","学时","选修、必修"};
        model.setColumnIdentifiers(columnNames);
        
        
    	Student s=new StudentDaoImpl().getByID(Integer.parseInt(Login.uid));
		List<Course>list=new ArrayList<Course>();
		list=new CourseDaoImpl().getStuAllCourses(s.getP_id());
		
		for (int i = list.size()-1; i >= 0; i--) {
			Course c1=(Course)list.get(i);
			Object[] obj={c1.getCourse_id(),c1.getCourse_name(),c1.getType(),c1.getTea_name(),c1.getStudytime(),c1.getCheck()};
			model.insertRow(table.getSelectedRowCount(), obj);
		}

		jButton2=new JButton("返回");
        jButton2.setBounds(500,600,100,40);
        jButton2.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jButton2);
        jButton2.addActionListener(this);
        
	    TableColumn column = null; 
	    int colunms = table.getColumnCount();  
	    for(int i1 = 0; i1 < colunms; i1++)  
	    {  
	        column = table.getColumnModel().getColumn(i1);  
	        column.setPreferredWidth(120);  
	     }  
         table.setFont( (new  Font("楷体",Font.CENTER_BASELINE,16)));
	     table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);  
	          
	    JScrollPane scroll = new JScrollPane(table);  
	    scroll.setBounds(20,100,650,450);
	    add(scroll);
    	setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    }
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO 自动生成的方法存根
		new Student_Login();
        dispose();
	}
}
