package cla.gui;


import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


import cla.entity.Course;
import cla.daoImpl.CourseDaoImpl;

public class Student_Login extends JFrame implements ActionListener{
    private JLabel jLabel;
    private JLabel jLabel1;
    private JLabel jLabel2;
   
    private JTextField jTextField;
    private JTextField jTextField2;
    private JTextArea jTextArea;
   
    private JButton jButton;
    private JButton jButton1;
    private JButton jButton2;
   
   
    public Student_Login(){
        InitComp();
    }
    public void InitComp(){
        setBounds(400,100,700,700);
        setTitle("课程管理");
        setLayout(null);
        jLabel=new JLabel("学生课程信息");
        jLabel.setFont( (new  Font("楷体",Font.BOLD,30)));
        jLabel.setBounds(230, 30, 250, 80);
        add(jLabel);
       
        jLabel1=new JLabel("课程名称：");
        jLabel1.setFont( (new  Font("楷体",Font.BOLD,16)));
        jLabel1.setBounds(180, 125, 140, 40);
        add(jLabel1);
       
        jTextField=new JTextField("");
        jTextField.setFont( (new  Font("楷体",Font.BOLD,16)));
        jTextField.setBounds(270,125,220,30);
        add(jTextField);
       
        jLabel2=new JLabel("课程类型：");
        jLabel2.setFont( (new  Font("楷体",Font.BOLD,16)));
        jLabel2.setBounds(180, 220, 140, 30);
        add(jLabel2);
       
        jTextField2=new JTextField("");
        jTextField2.setFont( (new  Font("楷体",Font.BOLD,16)));
        jTextField2.setBounds(270,220,220,30);
        add(jTextField2);
        
        jTextArea=new JTextArea("");
        jTextArea.setFont( (new  Font("楷体",Font.BOLD,16)));
        jTextArea.setBounds(50,300,600,100);
        add(jTextArea);
        
        jButton=new JButton("查询");
        jButton.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton.setBackground(Color.orange);
        jButton.setBounds(30,550,100,40);
        add(jButton);
        jButton.addActionListener(this);
        
        jButton1=new JButton("查看所有课程");
        jButton1.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton1.setBackground(Color.orange);
        jButton1.setBounds(250,550,180,40);
        add(jButton1);
        jButton1.addActionListener(this);
       
        jButton2=new JButton("返回");
        jButton2.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton2.setBackground(Color.orange);
        jButton2.setBounds(520,550,100,40);
        add(jButton2);
        jButton2.addActionListener(this);
       
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new Student_Login();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	if(e.getSource()==jButton){
    		Course c=new CourseDaoImpl().getByCourse(jTextField.getText(), jTextField2.getText());
    		jTextArea.append("课程名称："+c.getCourse_name()+", 教师名称："+c.getTea_name()+"\n\r 课程性质："+c.getType()+", 学时："+c.getStudytime()+", 选修/必修："+c.getCheck());
        }
    	if(e.getSource()==jButton1){
    		new StuAllCourse();
    		
        }
        if(e.getSource()==jButton2){
            new MainWindow();
            dispose();
        }
        
    }
}


