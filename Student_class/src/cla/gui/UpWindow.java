package cla.gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import cla.daoImpl.AdminDaoImpl;
import cla.daoImpl.CourseDaoImpl;
import cla.daoImpl.StudentDaoImpl;
import cla.daoImpl.TeacherDaoImpl;
import cla.entity.Course;
import cla.entity.Student;
import cla.entity.Teacher;

public class UpWindow extends JFrame implements ActionListener{

    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JButton jButton;
    private JButton jButton1;
    private JButton jButton2;
    
	private JTextField jTextField1;
    private JTextField jTextField2;
    private JTextField jTextField3;
    private JTextField jTextField4;
    private JTextField jTextField5;
    private JTextField jTextField;
    
    private JRadioButton jRadioButton;
    private JRadioButton jRadioButton2;
    private JRadioButton jRadioButton1;
    private ButtonGroup buttonGroup;
   
    public UpWindow(){
        InitComp();
    }
    public void InitComp(){
    	setBounds(400,100,700,700);
        setTitle("课程管理");
        setLayout(null);
        jTextField=new JTextField();
        jTextField.setBounds(100, 100, 330, 30);
        jTextField.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jTextField);
        
        jButton2=new JButton("查询");
        jButton2.setBounds(450, 100, 100, 30);
        jButton2.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton2.addActionListener(this);
        add(jButton2);
        
        buttonGroup=new ButtonGroup();
        jRadioButton=new JRadioButton("学生");
        jRadioButton.setBounds(110, 150, 70, 30);
        jRadioButton.setFont( (new  Font("楷体",Font.BOLD,14)));
        buttonGroup.add(jRadioButton);
        add(jRadioButton);
        
        jRadioButton1=new JRadioButton("教师");
        jRadioButton1.setBounds(250, 150, 70, 30);
        jRadioButton1.setFont( (new  Font("楷体",Font.BOLD,14)));
        buttonGroup.add(jRadioButton1);
        add(jRadioButton1);
        
        jRadioButton2=new JRadioButton("课程");
        jRadioButton2.setBounds(390, 150, 70, 30);
        jRadioButton2.setFont( (new  Font("楷体",Font.BOLD,14)));
        buttonGroup.add(jRadioButton2);
        add(jRadioButton2);
        
    	jLabel1=new JLabel("");
        jLabel1.setBounds(200, 180, 80, 30);
        jLabel1.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jLabel1);
        
        jTextField1=new JTextField();
        jTextField1.setBounds(280, 180, 150, 30);
        jTextField1.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jTextField1);
        
        jLabel2=new JLabel("");
        jLabel2.setBounds(200, 220, 80, 30);
        jLabel2.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jLabel2);
        
        jTextField2=new JTextField();
        jTextField2.setBounds(280, 220, 150, 30);
        jTextField2.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jTextField2);
        
        jLabel3=new JLabel("");
        jLabel3.setBounds(200, 260, 80, 30);
        jLabel3.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jLabel3);
        
        jTextField3=new JTextField();
        jTextField3.setBounds(280, 260, 150, 30);
        jTextField3.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jTextField3);
        
        jLabel4=new JLabel("");
        jLabel4.setBounds(200, 300, 80, 30);
        jLabel4.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jLabel4);
        
        jTextField4=new JTextField();
        jTextField4.setBounds(280, 300, 150, 30);
        jTextField4.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jTextField4);
        
        jLabel5=new JLabel("");
        jLabel5.setBounds(200, 340, 80, 30);
        jLabel5.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jLabel5);
        
        jTextField5=new JTextField();
        jTextField5.setBounds(280, 340, 150, 30);
        jTextField5.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jTextField5);
        
        jButton=new JButton("确认");
        jButton.setBounds(200, 390, 80, 30);
        jButton.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton.addActionListener(this);
        add(jButton);
        
        jButton1=new JButton("返回");
        jButton1.setBounds(360, 390, 80, 30);
        jButton1.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton1.addActionListener(this);
        add(jButton1);
        
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        

    }
    public static void main(String[] args) {
        new UpWindow();
    }
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO 自动生成的方法存根
		int cid=Integer.parseInt(jTextField.getText());
		if (e.getSource()==jButton1) {
			new Admin_Login();
			dispose();
		}
		if (e.getSource()==jButton2) {
			if(jRadioButton.isSelected()) {
				Student student=new StudentDaoImpl().getByID(cid);
				jLabel1.setText("学号");
				jLabel2.setText("姓名");
				jLabel3.setText("性别");
				jLabel4.setText("出生日期");
				jLabel5.setText("密码");
				jTextField1.setText(jTextField.getText());
				jTextField2.setText(student.getStuName());
				jTextField3.setText(student.getSex());
				jTextField4.setText(student.getBirthday());
				jTextField5.setText(student.getPassword());
			}else if (jRadioButton1.isSelected()) {
				Teacher student=new TeacherDaoImpl().getByID(cid);
				jLabel1.setText("教师工号");
				jLabel2.setText("姓名");
				jLabel3.setText("性别");
				jLabel4.setText("出生日期");
				jLabel5.setText("密码");
				jTextField1.setText(jTextField.getText());
				jTextField2.setText(student.getTea_name());
				jTextField3.setText(student.getSex());
				jTextField4.setText(student.getBirthday());
				jTextField5.setText(student.getPassword());
			}else if (jRadioButton2.isSelected()) {
				Course student=new CourseDaoImpl().getById(cid);
				jLabel1.setText("课程号");
				jLabel2.setText("课程名");
				jLabel3.setText("课程类型");
				jLabel4.setText("选修/必修");
				jLabel5.setText("学时");
				jTextField1.setText(jTextField.getText());
				jTextField2.setText(student.getCourse_name());
				jTextField3.setText(student.getType());
				jTextField4.setText(student.getCheck());
				jTextField5.setText(Integer.toString(student.getStudytime()));
			}
		}
		if (e.getSource()==jButton) {
			if (jRadioButton.isSelected()) {
			int result = new AdminDaoImpl().upByStuID(Integer.parseInt(jTextField1.getText()), jTextField2.getText(), jTextField3.getText(), jTextField4.getText(), jTextField5.getText());
			if (result==1) {
    			JOptionPane.showConfirmDialog(null, "学生信息更新成功");
			}else {
				JOptionPane.showConfirmDialog(null, "学生信息更新失败");
			}
		}else if (jRadioButton1.isSelected()) {
			int result = new AdminDaoImpl().upByTeaID(Integer.parseInt(jTextField1.getText()), jTextField2.getText(), jTextField3.getText(), jTextField4.getText(), jTextField5.getText());
			if (result==1) {
        		JOptionPane.showConfirmDialog(null, "教师信息更新成功");
			}else {
				JOptionPane.showConfirmDialog(null, "教师信息更新失败");
			}
		}else if (jRadioButton2.isSelected()) {
			int result = new AdminDaoImpl().upByCourID(Integer.parseInt(jTextField1.getText()), jTextField2.getText(), jTextField3.getText(), jTextField4.getText(), Integer.parseInt(jTextField5.getText()));
			if (result==1) {
        		JOptionPane.showConfirmDialog(null, "课程信息更新成功");
			}else {
				JOptionPane.showConfirmDialog(null, "课程信息更新失败");
			}
		}
		
	}
    
    
        
	}
}
