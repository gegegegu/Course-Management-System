package cla.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MainWindow extends JFrame implements ActionListener {

    JButton bt1;
    JButton bt2;
    JButton bt3;
    JButton bt4;
    JPanel panel;
    JPanel panel2;
    JLabel label;

    MainWindow() {
        this.setSize(900, 700);
        this.setTitle("课程管理系统");
        this.setLayout(null);
        this.setLocation(400, 200);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        bt1 = new JButton("学生选课信息");
        bt1.setSize(150, 50);
        bt1.setLocation(150, 400);
        bt1.setFont( (new  Font("楷体",Font.BOLD,14)));
        bt1.setBackground(Color.orange);
        bt1.addActionListener(this);
        bt1.setActionCommand("学生选课信息");

        bt2 = new JButton("管理员课程管理");
        bt2.setSize(150, 50);
        bt2.setLocation(150, 500);
        bt2.setFont( (new  Font("楷体",Font.BOLD,14)));
        bt2.setBackground(Color.orange);
        bt2.addActionListener(this);
        bt2.setActionCommand("管理员课程管理");

        bt3 = new JButton("教师课程信息");
        bt3.setSize(150, 50);
        bt3.setLocation(550, 400);
        bt3.setFont( (new  Font("楷体",Font.BOLD,14)));
        bt3.setBackground(Color.orange);
        bt3.addActionListener(this);
        bt3.setActionCommand("教师课程信息");

        bt4 = new JButton("退出");
        bt4.setSize(150, 50);
        bt4.setLocation(550, 500);
        bt4.setFont( (new  Font("楷体",Font.BOLD,14)));
        bt4.setBackground(Color.orange);
        bt4.addActionListener(this);
        bt4.setActionCommand("退出");

        this.add(bt1);
        this.add(bt2);
        this.add(bt3);
        this.add(bt4);

        panel = new JPanel();
        panel.setLocation(100, 20);
        panel.setLayout(null);
        
        this.add(panel);

        panel2 = new JPanel();
        panel2.setSize(650, 350);
        panel2.setLocation(100, 20);
        panel2.setLayout(null);
        panel2.setBackground(Color.orange);

        label = new JLabel();
        label.setText("欢迎登陆课程管理系统");
        label.setLocation(150, 60);
        label.setSize(500, 200);
        label.setFont( (new  Font("楷体",Font.BOLD,36)));
        panel2.add(label);
        this.add(panel2);
        panel2.setVisible(true);
        
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton bt = (JButton) e.getSource();
        if (bt != null) {
            this.remove(panel2);
            this.remove(panel);
        }
        if (bt.getText().equals("学生选课信息")) {
            new Login();
            panel.setLocation(100, 20);
            this.add(panel);
            this.repaint();
        } else {
            if (bt.getText().equals("管理员课程管理")) {
                new Login();
                panel.setLocation(100, 20);
                this.add(panel);
                this.repaint();
            } else {
                if (bt.getText().equals("教师课程信息")) {
                    new Login();
                    panel.setLocation(100, 20);
                    this.add(panel);
                    this.repaint();
                }else {
                    if(bt.getText().equals("退出")) {
                        dispose();
                    }
                }
            } 
        }
    }

    public static void main(String[] args) {
        new MainWindow();
    }
    

}
