package cla.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import cla.daoImpl.AdminDaoImpl;
import cla.daoImpl.StudentDaoImpl;
import cla.daoImpl.TeacherDaoImpl;
import cla.entity.Admin;
import cla.entity.Student;
import cla.entity.Teacher;


public class Login extends JFrame implements ActionListener{
	private JLabel jLabel;
    private JLabel jLabel1;
    private JLabel jLabel2;
   
    private JTextField jTextField;
    private JPasswordField jPasswordField;

    private JRadioButton jRadioButton;
    private JRadioButton jRadioButton1;
    private JRadioButton jRadioButton2;
    private ButtonGroup buttonGroup;
    
    private JButton jButton;
    private JButton jButton1;
    private JButton jButton2;
    
    public static String uid;
    public static String password;
    
    public Login(){
        InitComp();
    }
    
    public void InitComp(){
        setBounds(400,100,700,700);
        setTitle("课程管理");
        setLayout(null);
        jLabel=new JLabel("登录页面");
        jLabel.setBounds(260, 30, 190, 50);
        jLabel.setFont( (new  Font("楷体",Font.BOLD,26)));
        add(jLabel);
       
        jLabel1=new JLabel("用户名：");
        jLabel1.setBounds(180, 130, 140, 30);
        jLabel1.setFont( (new  Font("楷体",Font.BOLD,18)));
        add(jLabel1);
       
        jTextField=new JTextField("");
        jTextField.setBounds(280,130,200,30);
        jTextField.setFont( (new  Font("楷体",Font.BOLD,14)));
        add(jTextField);
        
        jLabel2=new JLabel("密  码：");
        jLabel2.setBounds(180, 200, 140, 30);
        jLabel2.setFont( (new  Font("楷体",Font.BOLD,18)));
        add(jLabel2);
       
        jPasswordField=new JPasswordField("");
        jPasswordField.setBounds(280,200,200,30);
        jPasswordField.setFont( (new  Font("楷体",Font.BOLD,14)));
        jPasswordField.setEchoChar('*');
        add(jPasswordField);
        
        buttonGroup =new ButtonGroup();
        jRadioButton=new JRadioButton("学生");
        jRadioButton.setBounds(170,250,150,30);
        jRadioButton.setFont( (new  Font("楷体",Font.BOLD,15)));
        buttonGroup.add(jRadioButton);
        
        jRadioButton1=new JRadioButton("老师");
        jRadioButton1.setBounds(320,250,150,30);
        jRadioButton1.setFont( (new  Font("楷体",Font.BOLD,15)));
        buttonGroup.add(jRadioButton1);
        
        jRadioButton2=new JRadioButton("管理员");
        jRadioButton2.setBounds(470,250,150,30);
        jRadioButton2.setFont( (new  Font("楷体",Font.BOLD,15)));
        buttonGroup.add(jRadioButton2);
        
        add(jRadioButton);
        add(jRadioButton1);
        add(jRadioButton2);
       
        jButton=new JButton("登录");
        jButton.setBounds(350,300,100,30);
        jButton.setFont( (new  Font("楷体",Font.BOLD,14)));
//        jButton.setIcon(new ImageIcon(Login.class.getResource("2.jpg")));
        jButton.setBackground(Color.orange);
        jButton.addActionListener(this);
        add(jButton);
        
        
        jButton1=new JButton("忘记密码");
        jButton1.setBounds(30,550,200,30);
        jButton1.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton1.setBackground(Color.orange);
//        add(jButton1);
       
        jButton2=new JButton("返回");
        jButton2.setBounds(460,300,100,30);
        jButton2.setFont( (new  Font("楷体",Font.BOLD,14)));
        jButton2.setBackground(Color.orange);
        add(jButton2);
        jButton2.addActionListener(this);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new Login();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	password=new String(jPasswordField.getPassword());
    	uid=jTextField.getText();
    	if(e.getSource()==jButton){
        	if (jRadioButton.isSelected()) {
            	int id=Integer.parseInt(jTextField.getText());
        		Student s=new StudentDaoImpl().getByID(id);
        		if (s.getPassword()==null) {
        			JOptionPane.showConfirmDialog(null, "用户名错误");
				}else if (s.getPassword().equals(password)) {
					new Student_Login();
					
					dispose();
				}else {
					JOptionPane.showConfirmDialog(null, "密码错误");
				}
        	}else if (jRadioButton1.isSelected()) {
            	int id=Integer.parseInt(jTextField.getText());
        		Teacher t=new TeacherDaoImpl().getByID(id);
        		if (t.getPassword()==null) {
        			JOptionPane.showConfirmDialog(null, "用户名错误");
				}else if (t.getPassword().equals(password)) {
					new Teacher_Login();
					dispose();
				}else {
					JOptionPane.showConfirmDialog(null, "密码错误");
				}
        	}else if (jRadioButton2.isSelected()) {
        		Admin a=new AdminDaoImpl().getByName(jTextField.getText());
        		if (a.getPassword()==null) {
        			JOptionPane.showConfirmDialog(null, "用户名错误");
				}else if (a.getPassword().equals(password)) {
					new Admin_Login();
					dispose();
				}else {
					JOptionPane.showConfirmDialog(null, "密码错误");
				}
        	}else {
        		JOptionPane.showConfirmDialog(null, "请选择您的角色");
        	}
        }
        if(e.getSource()==jButton2){
            dispose();
        }
    }
}
