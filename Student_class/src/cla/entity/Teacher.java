package cla.entity;

public class Teacher {
	private int tea_id;
	private String tea_name;
	private String sex;
	private int course_id;
	private String birthday;
	private String course_name;
	private String password;
	
	public Teacher() {
		
	}

	public Teacher(int course_id,String tea_name, String sex, String birthday, String course_name, String password) {
		this.setCourse_id(course_id);
		this.tea_name = tea_name;
		this.sex = sex;
		this.birthday = birthday;
		this.course_name = course_name;
		this.password = password;
	}
	
	public Teacher(int course_id,int tea_id, String tea_name, String sex, String birthday, String course_name, String password) {
		this.tea_name = tea_name;
		this.tea_id = tea_id;
		this.tea_name = tea_name;
		this.sex = sex;
		this.birthday = birthday;
		this.course_name = course_name;
		this.password = password;
	}

	public int getTea_id() {
		return tea_id;
	}

	public void setTea_id(int tea_id) {
		this.tea_id = tea_id;
	}

	public String getTea_name() {
		return tea_name;
	}

	public void setTea_name(String tea_name) {
		this.tea_name = tea_name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCourse_name() {
		return course_name;
	}

	public void setCourse_name(String class_name) {
		this.course_name = class_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getCourse_id() {
		return course_id;
	}

	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}
	
	
}
