package cla.entity;

public class Admin {
	private int id;
	private String adname;
	private String password;
	
	public Admin() {
	}

	public Admin(String adname, String password) {
		this.adname = adname;
		this.password = password;
	}

	public Admin(int id, String adname, String password) {
		this.id = id;
		this.adname = adname;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAdname() {
		return adname;
	}

	public void setAdname(String adname) {
		this.adname = adname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
}
