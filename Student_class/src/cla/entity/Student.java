package cla.entity;

public class Student {
	private int stuId;
	private String course_name;
	private String stuName;
	private String birthday;
	private String sex;
	private int p_id;
	private String positional;
	private int course_id;
	private int grade;
	private String password;
	public Student() {
	}
	public Student(String stuName,int stuId, String course_name, String birthday, String sex, int p_id, String positional,
			int course_id, int grade, String password) {
		this.stuName = stuName;
		this.stuId = stuId;
		this.course_name = course_name;
		this.birthday = birthday;
		this.sex = sex;
		this.p_id = p_id;
		this.positional = positional;
		this.course_id = course_id;
		this.grade = grade;
		this.password = password;
	}
	public int getStuId() {
		return stuId;
	}
	public void setStuId(int stuId) {
		this.stuId = stuId;
	}
	public String getCourse_name() {
		return course_name;
	}
	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public int getP_id() {
		return p_id;
	}
	public void setP_id(int p_id) {
		this.p_id = p_id;
	}
	public String getPositional() {
		return positional;
	}
	public void setPositional(String positional) {
		this.positional = positional;
	}
	public int getCourse_id() {
		return course_id;
	}
	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStuName() {
		return stuName;
	}
	public void setStuName(String stuName) {
		this.stuName = stuName;
	}
	
}
