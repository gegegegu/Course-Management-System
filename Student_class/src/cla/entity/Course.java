package cla.entity;

public class Course {
	private int course_id;
	private String course_name;
	private String type;
	private String check;
	private String tea_name;
	private int studytime;
	
	public Course() {
	}

	public Course(int course_id, String course_name, String type, String check, String tea_name, int studytime) {
		this.course_id = course_id;
		this.course_name = course_name;
		this.type = type;
		this.check = check;
		this.tea_name = tea_name;
		this.studytime = studytime;
	}
	
	public Course(String course_name, String type, String check, String tea_name, int studytime) {
		this.course_name = course_name;
		this.type = type;
		this.check = check;
		this.tea_name = tea_name;
		this.studytime = studytime;
	}
	public int getCourse_id() {
		return course_id;
	}
	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}
	public String getCourse_name() {
		return course_name;
	}
	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCheck() {
		return check;
	}
	public void setCheck(String check) {
		this.check = check;
	}
	public String getTea_name() {
		return tea_name;
	}
	public void setTea_name(String tea_name) {
		this.tea_name = tea_name;
	}
	public int getStudytime() {
		return studytime;
	}
	public void setStudytime(int studytime) {
		this.studytime = studytime;
	}
	
}
