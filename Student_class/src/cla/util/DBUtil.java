package cla.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUtil {
	 public static final String JDBC_DRIVER="com.mysql.jdbc.Driver";
	 public static final String DB_URL="jdbc:mysql://localhost:3306/class?useSSL=false";
	 
	 public static Connection getConnection() {
		 Connection conn=null;
		 try {
			Class.forName(JDBC_DRIVER);
			conn=DriverManager.getConnection(DB_URL,"root","123456");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;		
	 }
	 public static ResultSet query(String sql) {
		 Connection conn=null;
		 Statement st=null;
		 ResultSet rs=null;
		 conn=DBUtil.getConnection();
		 try {
			st=conn.createStatement();
			rs=st.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			//清空资源
		}		 
		return rs;	 
		 
	 }
	 public static int update(String sql) {
		 Connection conn=null;
		 Statement st=null;
		 int result=0;
		 conn=DBUtil.getConnection();
		 try {
			st=conn.createStatement();
			result=st.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			//清空资源
		}		 
		return result;
	 }

}
